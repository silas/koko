# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the koko package.
#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: koko\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-16 00:42+0000\n"
"PO-Revision-Date: 2021-10-26 14:12-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luiz Fernando Ranghetti"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "elchevive@opensuse.org"

#: main.cpp:56
#, kde-kuit-format
msgctxt "@title"
msgid "<application>Koko</application>"
msgstr "<application>Koko</application>"

#: main.cpp:58
#, kde-kuit-format
msgctxt "@title"
msgid "Koko is an image viewer for your image collection."
msgstr "O Koko é um visualizador de imagem para a sua coleção de imagens."

#: main.cpp:60
#, kde-kuit-format
msgctxt "@info:credit"
msgid "(c) 2013-2020 KDE Contributors"
msgstr "(c) 2013-2020 Colaboradores do KDE"

#: main.cpp:65
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Vishesh Handa"
msgstr "Vishesh Handa"

#: main.cpp:65 main.cpp:67 main.cpp:69 main.cpp:71 main.cpp:73 main.cpp:75
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Developer"
msgstr "Desenvolvedor"

#: main.cpp:67
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Atul Sharma"
msgstr "Atul Sharma"

#: main.cpp:69
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Marco Martin"
msgstr "Marco Martin"

#: main.cpp:71
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Nicolas Fella"
msgstr "Nicolas Fella"

#: main.cpp:73
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:75
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Mikel Johnson"
msgstr "Mikel Johnson"

#: main.cpp:82
#, kde-format
msgid "Image viewer"
msgstr "Visualizador de imagens"

#: main.cpp:83
#, kde-format
msgid "Reset the database"
msgstr "Reiniciar o banco de dados"

#: main.cpp:84
#, kde-format
msgid "path of image you want to open"
msgstr "caminho da imagem que você quer abrir"

#: notificationmanager.cpp:16
#, kde-format
msgid "Sharing failed"
msgstr "Falha ao compartilhar"

#: notificationmanager.cpp:22
#, kde-format
msgid "Shared url for image is <a href='%1'>%1</a>"
msgstr "A URL de compartilhamento da imagem é <a href='%1'>%1</a>"

#: qml/AlbumDelegate.qml:103
#, kde-format
msgid "1 Image"
msgid_plural "%1 Images"
msgstr[0] "1 imagem"
msgstr[1] "%1 imagens"

#: qml/AlbumView.qml:222 qml/AlbumView.qml:255
#, kde-format
msgid "Remove Bookmark"
msgstr "Remover favorito"

#: qml/AlbumView.qml:222 qml/AlbumView.qml:255
#, kde-format
msgctxt "@action:button Bookmarks the current folder"
msgid "Bookmark Folder"
msgstr "Favoritar pasta"

#: qml/AlbumView.qml:276
#, kde-format
msgid "Go Up"
msgstr "Ir pra cima"

#: qml/AlbumView.qml:307
#, kde-format
msgid "Home"
msgstr "Início"

#: qml/AlbumView.qml:307
#, kde-format
msgid "Root"
msgstr "Raiz"

#: qml/AlbumView.qml:325
#, kde-format
msgid "Select All"
msgstr "Selecionar tudo"

#: qml/AlbumView.qml:326
#, kde-format
msgid "Selects all the media in the current view"
msgstr "Seleciona todas as mídias na visualização atual"

#: qml/AlbumView.qml:333
#, kde-format
msgid "Deselect All"
msgstr "Desmarcar tudo"

#: qml/AlbumView.qml:334
#, kde-format
msgid "De-selects all the selected media"
msgstr "Desmarca todas as mídias selecionadas"

#: qml/AlbumView.qml:355
#, kde-format
msgid "Delete Selection"
msgstr "Excluir seleção"

#: qml/AlbumView.qml:356
#, kde-format
msgid "Move selected items to trash"
msgstr "Mover itens selecionados para a Lixeira"

#: qml/AlbumView.qml:362
#, kde-format
msgid "Restore Selection"
msgstr "Restaurar seleção"

#: qml/AlbumView.qml:363
#, kde-format
msgid "Restore selected items from trash"
msgstr "Restaurar itens selecionados da Lixeira"

#: qml/AlbumView.qml:370 qml/PlacesPage.qml:22
#, kde-format
msgid "Configure…"
msgstr "Configurar..."

#: qml/AlbumView.qml:501
#, kde-format
msgid "No Media Found"
msgstr "Nenhuma mídia encontrada"

#: qml/BottomNavBar.qml:67 qml/Sidebar.qml:118
#, kde-format
msgid "Pictures"
msgstr "Imagens"

#: qml/BottomNavBar.qml:73 qml/Sidebar.qml:128
#, kde-format
msgid "Videos"
msgstr "Vídeos"

#: qml/BottomNavBar.qml:79 qml/Sidebar.qml:122
#, kde-format
msgid "Favorites"
msgstr "Favoritos"

#: qml/BottomNavBar.qml:85 qml/Main.qml:53 qml/Sidebar.qml:113
#, kde-format
msgid "Places"
msgstr "Locais"

#: qml/EditorView.qml:26
#, kde-format
msgid "Edit"
msgstr "Editar"

#: qml/EditorView.qml:51
#, kde-format
msgctxt "@action:button Save image modification"
msgid "Save"
msgstr "Salvar"

#: qml/EditorView.qml:56 qml/EditorView.qml:242
#, kde-format
msgid ""
"Unable to save file. Check if you have the correct permission to edit this "
"file."
msgstr ""
"Não foi possível salvar o arquivo. Verifique se você tem as permissões "
"necessárias para editar este arquivo."

#: qml/EditorView.qml:65
#, kde-format
msgctxt "@action:button Undo modification"
msgid "Undo"
msgstr "Desfazer"

#: qml/EditorView.qml:77 qml/EditorView.qml:89
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Cancelar"

#: qml/EditorView.qml:77 qml/EditorView.qml:83
#, kde-format
msgctxt "@action:button Crop an image"
msgid "Crop"
msgstr "Recortar"

#: qml/EditorView.qml:89 qml/EditorView.qml:95
#, kde-format
msgctxt "@action:button Resize an image"
msgid "Resize"
msgstr "Redimensionar"

#: qml/EditorView.qml:101
#, kde-format
msgctxt "@action:button Rotate an image to the left"
msgid "Rotate left"
msgstr "Girar à esquerda"

#: qml/EditorView.qml:107
#, kde-format
msgctxt "@action:button Rotate an image to the right"
msgid "Rotate right"
msgstr "Girar à direita"

#: qml/EditorView.qml:113
#, kde-format
msgctxt "@action:button Mirror an image vertically"
msgid "Flip"
msgstr "Inverter"

#: qml/EditorView.qml:119
#, kde-format
msgctxt "@action:button Mirror an image horizontally"
msgid "Mirror"
msgstr "Espelhar"

#: qml/EditorView.qml:133
#, kde-format
msgctxt "@title:group for crop area size spinboxes"
msgid "Size:"
msgstr "Tamanho:"

#: qml/EditorView.qml:165
#, kde-format
msgctxt "@title:group for crop area position spinboxes"
msgid "Position:"
msgstr "Posição:"

#: qml/EditorView.qml:230
#, kde-format
msgid "Save As"
msgstr "Salvar como"

#: qml/EditorView.qml:238
#, kde-format
msgid "You are now editing a new file."
msgstr "Você está agora editando um novo arquivo."

#: qml/imagedelegate/VideoPlayer.qml:161
#, kde-format
msgid "Seek slider"
msgstr "Barra deslizante de posicionamento"

#: qml/imagedelegate/VideoPlayer.qml:203 qml/imagedelegate/VideoPlayer.qml:225
#, kde-format
msgid "Skip backward 1 second"
msgid_plural "Skip backward %1 seconds"
msgstr[0] "Voltar 1 segundo"
msgstr[1] "Voltar %1 segundos"

#: qml/imagedelegate/VideoPlayer.qml:213
#, kde-format
msgid "Pause playback"
msgstr "Pausar reprodução"

#: qml/imagedelegate/VideoPlayer.qml:213
#, kde-format
msgid "Continue playback"
msgstr "Continuar reprodução"

#: qml/imagedelegate/VideoPlayer.qml:235
#, kde-format
msgid "Unmute audio"
msgstr "Ativar som"

#: qml/imagedelegate/VideoPlayer.qml:235
#, kde-format
msgid "Mute audio"
msgstr "Desativar som"

#: qml/imagedelegate/VideoPlayer.qml:247
#, kde-format
msgid "Volume slider"
msgstr "Barra deslizante de volume"

#: qml/imagedelegate/VideoPlayer.qml:267
#, kde-format
msgid "Repeat current video"
msgstr "Repetir o vídeo atual"

#: qml/imagedelegate/VideoPlayer.qml:267
#, kde-format
msgid "Don't repeat current video"
msgstr "Não repetir o vídeo atual"

#: qml/ImageViewPage.qml:74
#, kde-format
msgid "Info"
msgstr "Informações"

#: qml/ImageViewPage.qml:76
#, kde-format
msgid "See information about video"
msgstr "Veja informações sobre o vídeo"

#: qml/ImageViewPage.qml:77
#, kde-format
msgid "See information about image"
msgstr "Veja informações sobre a imagem"

#: qml/ImageViewPage.qml:86
#, kde-format
msgid "Remove"
msgstr "Remover"

#: qml/ImageViewPage.qml:86
#, kde-format
msgid "Favorite"
msgstr "Favorito"

#: qml/ImageViewPage.qml:87
#, kde-format
msgid "Remove from favorites"
msgstr "Remover dos favoritos"

#: qml/ImageViewPage.qml:87
#, kde-format
msgid "Add to favorites"
msgstr "Adicionar aos favoritos"

#: qml/ImageViewPage.qml:98
#, kde-format
msgctxt "verb, edit an image"
msgid "Edit"
msgstr "Editar"

#: qml/ImageViewPage.qml:117
#, kde-format
msgid "Share Video"
msgstr "Compartilhar vídeo"

#: qml/ImageViewPage.qml:117
#, kde-format
msgid "Share Image"
msgstr "Compartilhar imagem"

#: qml/ImageViewPage.qml:118
#, kde-format
msgctxt "verb, share an image/video"
msgid "Share"
msgstr "Compartilhar"

#: qml/ImageViewPage.qml:132
#, kde-format
msgid "Start Slideshow"
msgstr "Iniciar apresentação de slides"

#: qml/ImageViewPage.qml:133
#, kde-format
msgid "Slideshow"
msgstr "Apresentação de slides"

#: qml/ImageViewPage.qml:140
#, kde-format
msgid "Thumbnail Bar"
msgstr "Barra de miniaturas"

#: qml/ImageViewPage.qml:141
#, kde-format
msgid "Hide Thumbnail Bar"
msgstr "Ocultar barra de miniaturas"

#: qml/ImageViewPage.qml:141
#, kde-format
msgid "Show Thumbnail Bar"
msgstr "Mostrar barra de miniaturas"

#: qml/ImageViewPage.qml:142
#, kde-format
msgid "Toggle Thumbnail Bar"
msgstr "Alternar barra de miniaturas"

#: qml/ImageViewPage.qml:157
#, kde-format
msgid "Fullscreen"
msgstr "Tela cheia"

#: qml/ImageViewPage.qml:157 qml/ImageViewPage.qml:158
#, kde-format
msgid "Exit Fullscreen"
msgstr "Sair da tela cheia"

#: qml/ImageViewPage.qml:158
#, kde-format
msgid "Enter Fullscreen"
msgstr "Entrar no modo de tela cheia"

#: qml/ImageViewPage.qml:411
#, kde-format
msgid "Previous image"
msgstr "Imagem anterior"

#: qml/ImageViewPage.qml:441
#, kde-format
msgid "Next image"
msgstr "Próxima imagem"

#: qml/ImageViewPage.qml:656 qml/SettingsPage.qml:54
#, kde-format
msgctxt "@label:spinbox Slideshow image changing interval"
msgid "Slideshow interval:"
msgstr "Intervalo da apresentação de slides:"

#: qml/ImageViewPage.qml:673 qml/SettingsPage.qml:62
#, kde-format
msgctxt "Slideshow image changing interval"
msgid "1 second"
msgid_plural "%1 seconds"
msgstr[0] "1 segundo"
msgstr[1] "%1 segundos"

#: qml/ImageViewPage.qml:726 qml/SettingsPage.qml:41
#, kde-format
msgctxt "@option:check"
msgid "Loop"
msgstr "Repetição"

#: qml/ImageViewPage.qml:732 qml/SettingsPage.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Randomize"
msgstr "Aleatório"

#: qml/ImageViewPage.qml:741
#, kde-format
msgid "Stop Slideshow"
msgstr "Parar apresentação de slides"

#: qml/ImageViewPage.qml:755
#, kde-format
msgid "Show All Controls"
msgstr "Mostrar todos os controles"

#: qml/InfoDrawer.qml:40 qml/InfoSidebar.qml:37
#, kde-format
msgid "Metadata"
msgstr "Metadados"

#: qml/InfoDrawerSidebarBase.qml:42
#, kde-format
msgid "File Name"
msgstr "Nome do arquivo"

#: qml/InfoDrawerSidebarBase.qml:52
#, kde-format
msgid "Dimension"
msgstr "Dimensão"

#: qml/InfoDrawerSidebarBase.qml:58
#, kde-format
msgctxt "dimensions"
msgid "%1 x %2"
msgstr "%1 x %2"

#: qml/InfoDrawerSidebarBase.qml:64
#, kde-format
msgid "Size"
msgstr "Tamanho"

#: qml/InfoDrawerSidebarBase.qml:76
#, kde-format
msgid "Created"
msgstr "Criado"

#: qml/InfoDrawerSidebarBase.qml:88
#, kde-format
msgid "Model"
msgstr "Modelo"

#: qml/InfoDrawerSidebarBase.qml:100
#, kde-format
msgid "Latitude"
msgstr "Latitude"

#: qml/InfoDrawerSidebarBase.qml:112
#, kde-format
msgid "Longitude"
msgstr "Longitude"

#: qml/InfoDrawerSidebarBase.qml:124
#, kde-format
msgid "Rating"
msgstr "Avaliação"

#: qml/InfoDrawerSidebarBase.qml:131
#, kde-format
msgid "Current rating %1"
msgstr "Avaliação atual %1"

#: qml/InfoDrawerSidebarBase.qml:138
#, kde-format
msgid "Set rating to %1"
msgstr "Definir avaliação como %1"

#: qml/InfoDrawerSidebarBase.qml:164
#, kde-format
msgid "Description"
msgstr "Descrição"

#: qml/InfoDrawerSidebarBase.qml:172
#, kde-format
msgid "Image description…"
msgstr "Descrição da imagem..."

#: qml/InfoDrawerSidebarBase.qml:182 qml/PlacesPage.qml:156 qml/Sidebar.qml:204
#, kde-format
msgid "Tags"
msgstr "Etiquetas"

#: qml/InfoDrawerSidebarBase.qml:198
#, kde-format
msgid "Add Tag"
msgstr "Adicionar etiqueta"

#: qml/InfoDrawerSidebarBase.qml:211
#, kde-format
msgid "Remove %1 tag"
msgstr "Remover etiqueta %1"

#: qml/Main.qml:106
#, kde-format
msgid "Images"
msgstr "Imagens"

#: qml/Main.qml:117 qml/Main.qml:126
#, kde-format
msgid "Folders"
msgstr "Pastas"

#: qml/PlacesPage.qml:79 qml/Sidebar.qml:158
#, kde-format
msgid "Network"
msgstr "Rede"

#: qml/PlacesPage.qml:85 qml/Sidebar.qml:149
#, kde-format
msgid "Trash"
msgstr "Lixeira"

#: qml/PlacesPage.qml:91
#, kde-format
msgid "Pinned Folders"
msgstr "Pastas fixadas"

#: qml/PlacesPage.qml:111 qml/Sidebar.qml:163
#, kde-format
msgid "Locations"
msgstr "Localizações"

#: qml/PlacesPage.qml:115 qml/Sidebar.qml:166
#, kde-format
msgid "Countries"
msgstr "Países"

#: qml/PlacesPage.qml:120 qml/Sidebar.qml:171
#, kde-format
msgid "States"
msgstr "Estados"

#: qml/PlacesPage.qml:125 qml/Sidebar.qml:176
#, kde-format
msgid "Cities"
msgstr "Cidades"

#: qml/PlacesPage.qml:131 qml/Sidebar.qml:181
#, kde-format
msgid "Time"
msgstr "Hora"

#: qml/PlacesPage.qml:135 qml/Sidebar.qml:184
#, kde-format
msgid "Years"
msgstr "Anos"

#: qml/PlacesPage.qml:140 qml/Sidebar.qml:189
#, kde-format
msgid "Months"
msgstr "Meses"

#: qml/PlacesPage.qml:145 qml/Sidebar.qml:194
#, kde-format
msgid "Weeks"
msgstr "Semanas"

#: qml/PlacesPage.qml:150 qml/Sidebar.qml:199
#, kde-format
msgid "Days"
msgstr "Dias"

#: qml/SettingsPage.qml:15 qml/Sidebar.qml:244
#, kde-format
msgid "Settings"
msgstr "Configurações"

#: qml/SettingsPage.qml:18
#, kde-format
msgid "General:"
msgstr "Geral:"

#: qml/SettingsPage.qml:19
#, kde-format
msgid "Show preview carousel in image view"
msgstr "Mostrar carrossel de visualização na visualização de imagens"

#: qml/SettingsPage.qml:24 qml/Sidebar.qml:227
#, kde-format
msgid "Thumbnails size:"
msgstr "Tamanho das miniaturas:"

#: qml/SettingsPage.qml:25 qml/Sidebar.qml:231
#, kde-format
msgid "%1 px"
msgstr "%1 px"

#: qml/SettingsPage.qml:40
#, kde-format
msgctxt "@title:group"
msgid "Slideshow settings:"
msgstr "Configurações da apresentação de slides:"

#: qml/SettingsPage.qml:115
#, kde-format
msgid "Open About Page"
msgstr "Abrir página Sobre"

#: qml/ShareAction.qml:22
#, kde-format
msgid "Share"
msgstr "Compartilhar"

#: qml/ShareAction.qml:23 qml/ShareDrawer.qml:22
#, kde-format
msgid "Share the selected media"
msgstr "Compartilhar as mídias selecionadas"

#: qml/Sidebar.qml:43
#, kde-format
msgid "Sort by"
msgstr "Ordenar por"

#: qml/Sidebar.qml:154
#, kde-format
msgctxt "Remote network locations"
msgid "Remote"
msgstr "Remoto"

#: qml/Tag.qml:84
#, kde-format
msgid "Remove Tag"
msgstr "Remover etiqueta"

#: qml/TagInput.qml:60
#, kde-format
msgid "Add new tag…"
msgstr "Adicionar nova etiqueta..."

#~ msgid "Edit image"
#~ msgstr "Editar imagem"

#, fuzzy
#~| msgctxt "@action:button Accept image modification"
#~| msgid "Accept"
#~ msgctxt "@action:button Accept crop for an image"
#~ msgid "Accept"
#~ msgstr "Aceitar"

#, fuzzy
#~| msgid "About"
#~ msgid "About"
#~ msgstr "Sobre"

#, fuzzy
#~| msgid "General:"
#~ msgid "General"
#~ msgstr "Geral:"

#~ msgid "New tag..."
#~ msgstr "Nova etiqueta..."

#~ msgid "Finished"
#~ msgstr "Concluído"

#~ msgctxt "verb, share an image"
#~ msgid "Share"
#~ msgstr "Compartilhar"

#~ msgid "By City"
#~ msgstr "Por cidade"

#~ msgid "By Day"
#~ msgstr "Por dia"

#~ msgid "Path"
#~ msgstr "Caminho"
