# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the koko package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: koko\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-05-16 00:42+0000\n"
"PO-Revision-Date: 2021-12-17 10:43+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Matjaž Jeran"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "matjaz.jeran@amis.net"

#: main.cpp:56
#, kde-kuit-format
msgctxt "@title"
msgid "<application>Koko</application>"
msgstr "<application>Koko</application>"

#: main.cpp:58
#, kde-kuit-format
msgctxt "@title"
msgid "Koko is an image viewer for your image collection."
msgstr "Koko je pregledovalnik slik za vašo zbirko slik."

#: main.cpp:60
#, kde-kuit-format
msgctxt "@info:credit"
msgid "(c) 2013-2020 KDE Contributors"
msgstr "(c) 2013-2020 sodelavci KDE"

#: main.cpp:65
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Vishesh Handa"
msgstr "Vishesh Handa"

#: main.cpp:65 main.cpp:67 main.cpp:69 main.cpp:71 main.cpp:73 main.cpp:75
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Developer"
msgstr "Razvijalec"

#: main.cpp:67
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Atul Sharma"
msgstr "Atul Sharma"

#: main.cpp:69
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Marco Martin"
msgstr "Marco Martin"

#: main.cpp:71
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Nicolas Fella"
msgstr "Nicolas Fella"

#: main.cpp:73
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Carl Schwan"
msgstr "Carl Schwan"

#: main.cpp:75
#, kde-kuit-format
msgctxt "@info:credit"
msgid "Mikel Johnson"
msgstr "Mikel Johnson"

#: main.cpp:82
#, kde-format
msgid "Image viewer"
msgstr "Pregledovalnik slik"

#: main.cpp:83
#, kde-format
msgid "Reset the database"
msgstr "Ponovno nastavi podatkovno bazo"

#: main.cpp:84
#, kde-format
msgid "path of image you want to open"
msgstr "pot do slike, ki jo želite odpreti"

#: notificationmanager.cpp:16
#, kde-format
msgid "Sharing failed"
msgstr "Deljenje ni uspelo"

#: notificationmanager.cpp:22
#, kde-format
msgid "Shared url for image is <a href='%1'>%1</a>"
msgstr "Deljeni spletni naslov za sliko je <a href='%1'>%1</a>"

#: qml/AlbumDelegate.qml:103
#, kde-format
msgid "1 Image"
msgid_plural "%1 Images"
msgstr[0] "%1 slika"
msgstr[1] "%1 sliki"
msgstr[2] "%1 slike"
msgstr[3] "%1 slik"

#: qml/AlbumView.qml:222 qml/AlbumView.qml:255
#, kde-format
msgid "Remove Bookmark"
msgstr "Odstrani zaznamek"

#: qml/AlbumView.qml:222 qml/AlbumView.qml:255
#, kde-format
msgctxt "@action:button Bookmarks the current folder"
msgid "Bookmark Folder"
msgstr "Mapa zaznamkov"

#: qml/AlbumView.qml:276
#, kde-format
msgid "Go Up"
msgstr "Pojdi gor"

#: qml/AlbumView.qml:307
#, kde-format
msgid "Home"
msgstr "Domov"

#: qml/AlbumView.qml:307
#, kde-format
msgid "Root"
msgstr "Koren"

#: qml/AlbumView.qml:325
#, kde-format
msgid "Select All"
msgstr "Izberi vse"

#: qml/AlbumView.qml:326
#, kde-format
msgid "Selects all the media in the current view"
msgstr "Izberi vse medije v trenutnem pogledu"

#: qml/AlbumView.qml:333
#, kde-format
msgid "Deselect All"
msgstr "Ne izberi nič"

#: qml/AlbumView.qml:334
#, kde-format
msgid "De-selects all the selected media"
msgstr "Prekliči izbiro vseh izbranih medijev"

#: qml/AlbumView.qml:355
#, kde-format
msgid "Delete Selection"
msgstr "Izbriši izbiro"

#: qml/AlbumView.qml:356
#, kde-format
msgid "Move selected items to trash"
msgstr "Prenesi izbrane predmete v smeti"

#: qml/AlbumView.qml:362
#, kde-format
msgid "Restore Selection"
msgstr "Obnovi izbiro"

#: qml/AlbumView.qml:363
#, kde-format
msgid "Restore selected items from trash"
msgstr "Obnovi izbrane predmete iz smeti"

#: qml/AlbumView.qml:370 qml/PlacesPage.qml:22
#, kde-format
msgid "Configure…"
msgstr "Nastavi…"

#: qml/AlbumView.qml:501
#, kde-format
msgid "No Media Found"
msgstr "Ni najdenih medijev"

#: qml/BottomNavBar.qml:67 qml/Sidebar.qml:118
#, kde-format
msgid "Pictures"
msgstr "Slike"

#: qml/BottomNavBar.qml:73 qml/Sidebar.qml:128
#, kde-format
msgid "Videos"
msgstr "Videoposnetki"

#: qml/BottomNavBar.qml:79 qml/Sidebar.qml:122
#, kde-format
msgid "Favorites"
msgstr "Priljubljeni"

#: qml/BottomNavBar.qml:85 qml/Main.qml:53 qml/Sidebar.qml:113
#, kde-format
msgid "Places"
msgstr "Kraji"

#: qml/EditorView.qml:26
#, kde-format
msgid "Edit"
msgstr "Uredi"

#: qml/EditorView.qml:51
#, kde-format
msgctxt "@action:button Save image modification"
msgid "Save"
msgstr "Shrani"

#: qml/EditorView.qml:56 qml/EditorView.qml:242
#, kde-format
msgid ""
"Unable to save file. Check if you have the correct permission to edit this "
"file."
msgstr ""
"Ni mogoče shraniti datoteke. Preverite ali imata ustrezna dovoljenja za "
"urejanje te datoteke."

#: qml/EditorView.qml:65
#, kde-format
msgctxt "@action:button Undo modification"
msgid "Undo"
msgstr "Razveljavi"

#: qml/EditorView.qml:77 qml/EditorView.qml:89
#, kde-format
msgctxt "@action:button"
msgid "Cancel"
msgstr "Prekliči"

#: qml/EditorView.qml:77 qml/EditorView.qml:83
#, kde-format
msgctxt "@action:button Crop an image"
msgid "Crop"
msgstr "Obreži"

#: qml/EditorView.qml:89 qml/EditorView.qml:95
#, kde-format
msgctxt "@action:button Resize an image"
msgid "Resize"
msgstr "Spremeni velikost"

#: qml/EditorView.qml:101
#, kde-format
msgctxt "@action:button Rotate an image to the left"
msgid "Rotate left"
msgstr "Zasukaj levo"

#: qml/EditorView.qml:107
#, kde-format
msgctxt "@action:button Rotate an image to the right"
msgid "Rotate right"
msgstr "Zasukaj desno"

#: qml/EditorView.qml:113
#, kde-format
msgctxt "@action:button Mirror an image vertically"
msgid "Flip"
msgstr "Obrni"

#: qml/EditorView.qml:119
#, kde-format
msgctxt "@action:button Mirror an image horizontally"
msgid "Mirror"
msgstr "Prezrcali"

#: qml/EditorView.qml:133
#, kde-format
msgctxt "@title:group for crop area size spinboxes"
msgid "Size:"
msgstr "Velikost:"

#: qml/EditorView.qml:165
#, kde-format
msgctxt "@title:group for crop area position spinboxes"
msgid "Position:"
msgstr "Pozicija:"

#: qml/EditorView.qml:230
#, kde-format
msgid "Save As"
msgstr "Shrani kot"

#: qml/EditorView.qml:238
#, kde-format
msgid "You are now editing a new file."
msgstr "Zdaj urejate novo datoteko."

#: qml/imagedelegate/VideoPlayer.qml:161
#, kde-format
msgid "Seek slider"
msgstr "Drsnik iskanja"

#: qml/imagedelegate/VideoPlayer.qml:203 qml/imagedelegate/VideoPlayer.qml:225
#, kde-format
msgid "Skip backward 1 second"
msgid_plural "Skip backward %1 seconds"
msgstr[0] "Preskoči nazaj za %1 sekundo"
msgstr[1] "Preskoči nazaj za %1 sekundi"
msgstr[2] "Preskoči nazaj za %1 sekunde"
msgstr[3] "Preskoči nazaj za %1 sekund"

#: qml/imagedelegate/VideoPlayer.qml:213
#, kde-format
msgid "Pause playback"
msgstr "Premor za playback"

#: qml/imagedelegate/VideoPlayer.qml:213
#, kde-format
msgid "Continue playback"
msgstr "Nadaljuj playback"

#: qml/imagedelegate/VideoPlayer.qml:235
#, kde-format
msgid "Unmute audio"
msgstr "Sprosti zvok"

#: qml/imagedelegate/VideoPlayer.qml:235
#, kde-format
msgid "Mute audio"
msgstr "Utišaj zvok"

#: qml/imagedelegate/VideoPlayer.qml:247
#, kde-format
msgid "Volume slider"
msgstr "Drsnik glasnosti"

#: qml/imagedelegate/VideoPlayer.qml:267
#, kde-format
msgid "Repeat current video"
msgstr "Ponovi trenutni video"

#: qml/imagedelegate/VideoPlayer.qml:267
#, kde-format
msgid "Don't repeat current video"
msgstr "Ne ponavljaj trenutnega videa"

#: qml/ImageViewPage.qml:74
#, kde-format
msgid "Info"
msgstr "Informacije"

#: qml/ImageViewPage.qml:76
#, kde-format
msgid "See information about video"
msgstr "Glej informacije o videoposnetku"

#: qml/ImageViewPage.qml:77
#, kde-format
msgid "See information about image"
msgstr "Glej informacije o sliki"

#: qml/ImageViewPage.qml:86
#, kde-format
msgid "Remove"
msgstr "Odstrani"

#: qml/ImageViewPage.qml:86
#, kde-format
msgid "Favorite"
msgstr "Priljubljen"

#: qml/ImageViewPage.qml:87
#, kde-format
msgid "Remove from favorites"
msgstr "Odstrani iz priljubljenih"

#: qml/ImageViewPage.qml:87
#, kde-format
msgid "Add to favorites"
msgstr "Dodaj k priljubljenim"

#: qml/ImageViewPage.qml:98
#, kde-format
msgctxt "verb, edit an image"
msgid "Edit"
msgstr "Uredi"

#: qml/ImageViewPage.qml:117
#, kde-format
msgid "Share Video"
msgstr "Deli videoposnetek"

#: qml/ImageViewPage.qml:117
#, kde-format
msgid "Share Image"
msgstr "Deli sliko"

#: qml/ImageViewPage.qml:118
#, kde-format
msgctxt "verb, share an image/video"
msgid "Share"
msgstr "Deli"

#: qml/ImageViewPage.qml:132
#, kde-format
msgid "Start Slideshow"
msgstr "Začni predstavitev"

#: qml/ImageViewPage.qml:133
#, kde-format
msgid "Slideshow"
msgstr "Predstavitev"

#: qml/ImageViewPage.qml:140
#, kde-format
msgid "Thumbnail Bar"
msgstr "Vrstica predoglednih sličic"

#: qml/ImageViewPage.qml:141
#, kde-format
msgid "Hide Thumbnail Bar"
msgstr "Skrij vrstico predoglednih sličic"

#: qml/ImageViewPage.qml:141
#, kde-format
msgid "Show Thumbnail Bar"
msgstr "Prikaži vrstico predoglednih sličic"

#: qml/ImageViewPage.qml:142
#, kde-format
msgid "Toggle Thumbnail Bar"
msgstr "Preklopi vrstico predoglednih sličic"

#: qml/ImageViewPage.qml:157
#, kde-format
msgid "Fullscreen"
msgstr "Celotni zaslon"

#: qml/ImageViewPage.qml:157 qml/ImageViewPage.qml:158
#, kde-format
msgid "Exit Fullscreen"
msgstr "Izhod iz celotnega zaslona"

#: qml/ImageViewPage.qml:158
#, kde-format
msgid "Enter Fullscreen"
msgstr "Vstopi v celotni zaslon"

#: qml/ImageViewPage.qml:411
#, kde-format
msgid "Previous image"
msgstr "Prejšnja slika"

#: qml/ImageViewPage.qml:441
#, kde-format
msgid "Next image"
msgstr "Naslednja slika"

#: qml/ImageViewPage.qml:656 qml/SettingsPage.qml:54
#, kde-format
msgctxt "@label:spinbox Slideshow image changing interval"
msgid "Slideshow interval:"
msgstr "Interval med prikazi predstavitve:"

#: qml/ImageViewPage.qml:673 qml/SettingsPage.qml:62
#, kde-format
msgctxt "Slideshow image changing interval"
msgid "1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekunda"
msgstr[1] "%1 sekundi"
msgstr[2] "%1 sekunde"
msgstr[3] "%1 sekund"

#: qml/ImageViewPage.qml:726 qml/SettingsPage.qml:41
#, kde-format
msgctxt "@option:check"
msgid "Loop"
msgstr "Zanka"

#: qml/ImageViewPage.qml:732 qml/SettingsPage.qml:48
#, kde-format
msgctxt "@option:check"
msgid "Randomize"
msgstr "Slučajna razvrstitev"

#: qml/ImageViewPage.qml:741
#, kde-format
msgid "Stop Slideshow"
msgstr "Ustavi predstavitev"

#: qml/ImageViewPage.qml:755
#, kde-format
msgid "Show All Controls"
msgstr "Prikaži vse kontrole"

#: qml/InfoDrawer.qml:40 qml/InfoSidebar.qml:37
#, kde-format
msgid "Metadata"
msgstr "Metapodatki"

#: qml/InfoDrawerSidebarBase.qml:42
#, kde-format
msgid "File Name"
msgstr "Ime datoteke"

#: qml/InfoDrawerSidebarBase.qml:52
#, kde-format
msgid "Dimension"
msgstr "Dimenzija"

#: qml/InfoDrawerSidebarBase.qml:58
#, kde-format
msgctxt "dimensions"
msgid "%1 x %2"
msgstr "%1 x %2"

#: qml/InfoDrawerSidebarBase.qml:64
#, kde-format
msgid "Size"
msgstr "Velikost"

#: qml/InfoDrawerSidebarBase.qml:76
#, kde-format
msgid "Created"
msgstr "Ustvarjeno"

#: qml/InfoDrawerSidebarBase.qml:88
#, kde-format
msgid "Model"
msgstr "Model"

#: qml/InfoDrawerSidebarBase.qml:100
#, kde-format
msgid "Latitude"
msgstr "Širina"

#: qml/InfoDrawerSidebarBase.qml:112
#, kde-format
msgid "Longitude"
msgstr "Dolžina"

#: qml/InfoDrawerSidebarBase.qml:124
#, kde-format
msgid "Rating"
msgstr "Ocena"

#: qml/InfoDrawerSidebarBase.qml:131
#, kde-format
msgid "Current rating %1"
msgstr "Trenutna ocena %1"

#: qml/InfoDrawerSidebarBase.qml:138
#, kde-format
msgid "Set rating to %1"
msgstr "Nastavi oceno na %1"

#: qml/InfoDrawerSidebarBase.qml:164
#, kde-format
msgid "Description"
msgstr "Opis"

#: qml/InfoDrawerSidebarBase.qml:172
#, kde-format
msgid "Image description…"
msgstr "Opis slike…"

#: qml/InfoDrawerSidebarBase.qml:182 qml/PlacesPage.qml:156 qml/Sidebar.qml:204
#, kde-format
msgid "Tags"
msgstr "Oznake"

#: qml/InfoDrawerSidebarBase.qml:198
#, kde-format
msgid "Add Tag"
msgstr "Dodaj oznako"

#: qml/InfoDrawerSidebarBase.qml:211
#, kde-format
msgid "Remove %1 tag"
msgstr "Odstrani oznako %1"

#: qml/Main.qml:106
#, kde-format
msgid "Images"
msgstr "Slike"

#: qml/Main.qml:117 qml/Main.qml:126
#, kde-format
msgid "Folders"
msgstr "Mape"

#: qml/PlacesPage.qml:79 qml/Sidebar.qml:158
#, kde-format
msgid "Network"
msgstr "Omrežje"

#: qml/PlacesPage.qml:85 qml/Sidebar.qml:149
#, kde-format
msgid "Trash"
msgstr "Smeti"

#: qml/PlacesPage.qml:91
#, kde-format
msgid "Pinned Folders"
msgstr "Pripete mape"

#: qml/PlacesPage.qml:111 qml/Sidebar.qml:163
#, kde-format
msgid "Locations"
msgstr "Lokacije"

#: qml/PlacesPage.qml:115 qml/Sidebar.qml:166
#, kde-format
msgid "Countries"
msgstr "Države"

#: qml/PlacesPage.qml:120 qml/Sidebar.qml:171
#, kde-format
msgid "States"
msgstr "Zvezne države"

#: qml/PlacesPage.qml:125 qml/Sidebar.qml:176
#, kde-format
msgid "Cities"
msgstr "Mesta"

#: qml/PlacesPage.qml:131 qml/Sidebar.qml:181
#, kde-format
msgid "Time"
msgstr "Čas"

#: qml/PlacesPage.qml:135 qml/Sidebar.qml:184
#, kde-format
msgid "Years"
msgstr "Leta"

#: qml/PlacesPage.qml:140 qml/Sidebar.qml:189
#, kde-format
msgid "Months"
msgstr "Meseci"

#: qml/PlacesPage.qml:145 qml/Sidebar.qml:194
#, kde-format
msgid "Weeks"
msgstr "Tedni"

#: qml/PlacesPage.qml:150 qml/Sidebar.qml:199
#, kde-format
msgid "Days"
msgstr "Dnevi"

#: qml/SettingsPage.qml:15 qml/Sidebar.qml:244
#, kde-format
msgid "Settings"
msgstr "Nastavitve"

#: qml/SettingsPage.qml:18
#, kde-format
msgid "General:"
msgstr "Splošno:"

#: qml/SettingsPage.qml:19
#, kde-format
msgid "Show preview carousel in image view"
msgstr "Prikaži predogledni kolut v prikazu slike"

#: qml/SettingsPage.qml:24 qml/Sidebar.qml:227
#, kde-format
msgid "Thumbnails size:"
msgstr "Velikost predogledne sličice:"

#: qml/SettingsPage.qml:25 qml/Sidebar.qml:231
#, kde-format
msgid "%1 px"
msgstr "%1 px"

#: qml/SettingsPage.qml:40
#, kde-format
msgctxt "@title:group"
msgid "Slideshow settings:"
msgstr "Nastavitve predstavitve:"

#: qml/SettingsPage.qml:115
#, kde-format
msgid "Open About Page"
msgstr "Odpri stran o programu"

#: qml/ShareAction.qml:22
#, kde-format
msgid "Share"
msgstr "Deli"

#: qml/ShareAction.qml:23 qml/ShareDrawer.qml:22
#, kde-format
msgid "Share the selected media"
msgstr "Deli izbrane medije"

#: qml/Sidebar.qml:43
#, kde-format
msgid "Sort by"
msgstr "Uredi po"

#: qml/Sidebar.qml:154
#, kde-format
msgctxt "Remote network locations"
msgid "Remote"
msgstr "Oddaljeni"

#: qml/Tag.qml:84
#, kde-format
msgid "Remove Tag"
msgstr "Odstrani oznako"

#: qml/TagInput.qml:60
#, kde-format
msgid "Add new tag…"
msgstr "Dodaj novo oznako…"

#~ msgid "Edit image"
#~ msgstr "Uredi sliko"

#~ msgctxt "@action:button Accept crop for an image"
#~ msgid "Accept"
#~ msgstr "Sprejmi"

#~ msgid "About"
#~ msgstr "O programu"

#~ msgid "General"
#~ msgstr "Splošno"

#~ msgid "New tag..."
#~ msgstr "Nova oznaka..."

#~ msgid "Finished"
#~ msgstr "Končano"

#~ msgctxt "verb, share an image"
#~ msgid "Share"
#~ msgstr "Deli"

#~ msgid "By City"
#~ msgstr "Po kraju"

#~ msgid "By Day"
#~ msgstr "Po dnevu"

#~ msgid "Path"
#~ msgstr "Pot"
