// SPDX-FileCopyrightText: 2021 Arjen Hiemstra <ahiemstra@heimr.nl>
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

import org.kde.kirigami 2.15 as Kirigami

Kirigami.AboutPage {
    aboutData: kokoAboutData
}
